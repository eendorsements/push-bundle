<?php

namespace EEN\PushBundle\Util;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ReviewShareService
 * @package EEN\PushBundle\Util
 */
class ReviewShareService
{
    private $resolver;

    /**
     * ReviewPushService constructor.
     */
    public function __construct()
    {
        // configure share services' options
        $this->resolver = new OptionsResolver();
        $this->configureOptions($this->resolver);
    }

    /**
     * @param array $reviewShareSites
     * @return array
     */
    public function shareReview(array $reviewShareSites)
    {
        // resolve options
        foreach ($reviewShareSites as $shareSiteOptions) {
            $this->resolver->resolve($shareSiteOptions);
        }

        // init cURL
        $ch = curl_init();

        // set shared curl options
        $options = [
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_NOBODY => 1,
            CURLOPT_CONNECTTIMEOUT => 1,
            CURLOPT_TIMEOUT => 1,
        ];
        curl_setopt_array($ch, $options);

        // initialize array for storing responses
        $responses = [];

        // send review to each share site
        foreach ($reviewShareSites as $shareSite => $shareSiteOptions) {
            // create POST data
            $data = [
                'access_token' => $shareSiteOptions['access_token'],
                'message' => $shareSiteOptions['content']['message'],
            ];

            // check if link is included
            if (array_key_exists('link', $shareSiteOptions['content'])) {
                $data['link'] = $shareSiteOptions['content']['link'];
            }

            // build query string
            $postQueryData = http_build_query($data);

            // set URL
            $url = $shareSiteOptions['url'].'?'.$postQueryData;
            curl_setopt($ch, CURLOPT_URL, $url);

            // set request to POST
            curl_setopt($ch, CURLOPT_POST, 1);

            // get response
            $response = curl_exec($ch);

            // check for cURL errors
            if (false === $response) {
                $responses[$shareSite] = [
                    'status_code' => 500,
                    'detail' => 'cURL error: '.curl_errno($ch).' message: '.curl_error($ch),
                ];

                // process next service
                continue;
            }

            // get status code
            $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            // store share site response
            $responses[$shareSite] = [
                'status_code' => $statusCode,
                'detail' => Response::$statusTexts[$statusCode],
            ];
        }

        // close cURL handle
        curl_close($ch);

        // return share site responses
        return $responses;
    }

    private function configureOptions(OptionsResolver $resolver)
    {
        // set required options
        $resolver->setRequired(['url', 'access_token', 'content']);

        // set allowed types for each option
        $resolver->setAllowedTypes('url', 'string');
        $resolver->setAllowedTypes('access_token', 'string');
        $resolver->setAllowedTypes('content', 'array');
    }
}
