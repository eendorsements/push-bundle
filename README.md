Installation
============

Step 1: Add Required Repo
-------------------------

Open your project composer.json file and add the following:

```json
{
    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:eendorsements/push-bundle.git"
        }
    ]
}
```

Step 2: Download the Bundle
---------------------------

Open a command console, enter your project directory and execute the
following command to download the latest stable version of this bundle:

```bash
$ composer require eendorsements/push-bundle
```

This command requires you to have Composer installed globally, as explained
in the [installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

Step 3: Enable the Bundle
-------------------------

Then, enable the bundle by adding it to the list of registered bundles
in the `app/AppKernel.php` file of your project:

```php
<?php
// app/AppKernel.php

// ...
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            // ...

            new EEN\PushBundle\EENPushBundle(),
        );

        // ...
    }

    // ...
}
```